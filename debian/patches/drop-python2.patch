Description: Drop support for Python 2, clean up imports and dependencies
Origin: upstream, https://github.com/anhaidgroup/py_stringmatching/pull/85
Author: Zachary Ware @zware <zachary.ware@gmail.com>
Last-Update: 2023-01-04

--- a/README.rst
+++ b/README.rst
@@ -20,8 +20,8 @@
 
 py_stringmatching has been tested on each Python version between 3.7 and 3.11, inclusive.
 
-The required dependencies to build the package are NumPy 1.7.0 or higher,
-Six, and a C or C++ compiler. For the development version, you will also need Cython.
+The required dependencies to build the package are NumPy 1.7.0 or higher
+and a C or C++ compiler. For the development version, you will also need Cython.
 
 Platforms
 =========
--- a/build_tools/requirements_dev.txt
+++ b/build_tools/requirements_dev.txt
@@ -1,3 +1,2 @@
 numpy>=1.7.0
-six
 Cython
--- a/docs/Installation.rst
+++ b/docs/Installation.rst
@@ -14,7 +14,6 @@
 Dependencies
 ------------
     * numpy 1.7.0 or higher
-    * six
 
 .. note::
 
--- a/py_stringmatching/similarity_measure/affine.py
+++ b/py_stringmatching/similarity_measure/affine.py
@@ -1,6 +1,5 @@
 
 from py_stringmatching import utils
-from six.moves import xrange
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
 from py_stringmatching.similarity_measure.cython.cython_affine import affine
--- a/py_stringmatching/similarity_measure/bag_distance.py
+++ b/py_stringmatching/similarity_measure/bag_distance.py
@@ -1,6 +1,5 @@
 """Bag distance measure"""
 
-from __future__ import division
 import collections
 
 from py_stringmatching import utils
--- a/py_stringmatching/similarity_measure/cython/cython_levenshtein.pyx
+++ b/py_stringmatching/similarity_measure/cython/cython_levenshtein.pyx
@@ -1,6 +1,5 @@
 # cython: boundscheck=False
 
-from __future__ import division
 import cython
 import numpy as np
 cimport numpy as np
--- a/py_stringmatching/similarity_measure/editex.py
+++ b/py_stringmatching/similarity_measure/editex.py
@@ -1,16 +1,11 @@
 # coding=utf-8
 """Editex distance measure"""
 
-from __future__ import division
-from __future__ import unicode_literals
 import unicodedata
-import six
 
 import numpy as np
 
 from py_stringmatching import utils
-from six.moves import xrange
-from six import text_type
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
 
@@ -79,8 +74,8 @@
             return 0
 
         # convert both the strings to NFKD normalized unicode
-        string1 = unicodedata.normalize('NFKD', text_type(string1.upper()))
-        string2 = unicodedata.normalize('NFKD', text_type(string2.upper()))
+        string1 = unicodedata.normalize('NFKD', string1.upper())
+        string2 = unicodedata.normalize('NFKD', string2.upper())
 
         # convert ß to SS (for Python2)
         string1 = string1.replace('ß', 'SS')
@@ -100,16 +95,16 @@
                                      self.group_cost)
 
         if not self.local:
-            for i in xrange(1, len1 + 1):
+            for i in range(1, len1 + 1):
                 d_mat[i, 0] = d_mat[i - 1, 0] + editex_helper.d_cost(
                                                     string1[i - 1], string1[i])
 
-        for j in xrange(1, len2 + 1):
+        for j in range(1, len2 + 1):
             d_mat[0, j] = d_mat[0, j - 1] + editex_helper.d_cost(string2[j - 1],
                                                                  string2[j])
 
-        for i in xrange(1, len1 + 1):
-            for j in xrange(1, len2 + 1):
+        for i in range(1, len1 + 1):
+            for j in range(1, len2 + 1):
                 d_mat[i, j] = min(d_mat[i - 1, j] + editex_helper.d_cost(
                                                     string1[i - 1], string1[i]),
                                   d_mat[i, j - 1] + editex_helper.d_cost(
--- a/py_stringmatching/similarity_measure/hamming_distance.py
+++ b/py_stringmatching/similarity_measure/hamming_distance.py
@@ -1,5 +1,3 @@
-from __future__ import division
-
 from py_stringmatching import utils
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
--- a/py_stringmatching/similarity_measure/jaro.py
+++ b/py_stringmatching/similarity_measure/jaro.py
@@ -1,5 +1,4 @@
 from py_stringmatching import utils
-from six.moves import xrange
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
 from py_stringmatching.similarity_measure.cython.cython_jaro import jaro
--- a/py_stringmatching/similarity_measure/levenshtein.py
+++ b/py_stringmatching/similarity_measure/levenshtein.py
@@ -1,5 +1,3 @@
-from __future__ import division
-
 from py_stringmatching import utils
 from py_stringmatching.similarity_measure.cython.cython_levenshtein import levenshtein
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
--- a/py_stringmatching/similarity_measure/needleman_wunsch.py
+++ b/py_stringmatching/similarity_measure/needleman_wunsch.py
@@ -1,7 +1,4 @@
-import numpy as np
-
 from py_stringmatching import utils
-from six.moves import xrange
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
 from py_stringmatching.similarity_measure.cython.cython_needleman_wunsch import needleman_wunsch
--- a/py_stringmatching/similarity_measure/partial_ratio.py
+++ b/py_stringmatching/similarity_measure/partial_ratio.py
@@ -1,7 +1,5 @@
 """Fuzzy Wuzzy Partial Ratio Similarity Measure"""
 
-from __future__ import division
-
 from difflib import SequenceMatcher
 from py_stringmatching import utils
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
--- a/py_stringmatching/similarity_measure/partial_token_sort.py
+++ b/py_stringmatching/similarity_measure/partial_token_sort.py
@@ -1,8 +1,5 @@
 """Fuzzy Wuzzy Token Sort Similarity Measure"""
 
-from __future__ import division
-
-from difflib import SequenceMatcher
 from py_stringmatching import utils
 
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
--- a/py_stringmatching/similarity_measure/ratio.py
+++ b/py_stringmatching/similarity_measure/ratio.py
@@ -1,7 +1,5 @@
 """Fuzzy Wuzzy Ratio Similarity Measure"""
 
-from __future__ import division
-
 from difflib import SequenceMatcher
 from py_stringmatching import utils
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
--- a/py_stringmatching/similarity_measure/smith_waterman.py
+++ b/py_stringmatching/similarity_measure/smith_waterman.py
@@ -2,7 +2,6 @@
 
 from py_stringmatching.similarity_measure.cython.cython_utils import cython_sim_ident
 from py_stringmatching import utils
-from six.moves import xrange
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
                                                     SequenceSimilarityMeasure
 from py_stringmatching.similarity_measure.cython.cython_smith_waterman import smith_waterman
--- a/py_stringmatching/similarity_measure/soft_tfidf.py
+++ b/py_stringmatching/similarity_measure/soft_tfidf.py
@@ -1,4 +1,3 @@
-from __future__ import division
 from math import sqrt
 import collections
 
--- a/py_stringmatching/similarity_measure/tfidf.py
+++ b/py_stringmatching/similarity_measure/tfidf.py
@@ -1,4 +1,3 @@
-from __future__ import division
 from math import log, sqrt
 import collections
 
--- a/py_stringmatching/similarity_measure/token_sort.py
+++ b/py_stringmatching/similarity_measure/token_sort.py
@@ -1,8 +1,5 @@
 """Fuzzy Wuzzy Token Sort Similarity Measure"""
 
-from __future__ import division
-
-from difflib import SequenceMatcher
 from py_stringmatching import utils
 
 from py_stringmatching.similarity_measure.sequence_similarity_measure import \
--- a/py_stringmatching/tests/test_sim_Soundex.py
+++ b/py_stringmatching/tests/test_sim_Soundex.py
@@ -1,8 +1,5 @@
 # coding=utf-8
 
-from __future__ import unicode_literals
-
-import math
 import unittest
 
 from py_stringmatching.similarity_measure.soundex import Soundex
--- a/py_stringmatching/tests/test_simfunctions.py
+++ b/py_stringmatching/tests/test_simfunctions.py
@@ -1,8 +1,5 @@
-
 # coding=utf-8
 
-from __future__ import unicode_literals
-
 import math
 import unittest
 
--- a/py_stringmatching/tests/test_tokenizers.py
+++ b/py_stringmatching/tests/test_tokenizers.py
@@ -1,5 +1,3 @@
-from __future__ import unicode_literals
-
 import unittest
 
 from py_stringmatching.tokenizer.alphabetic_tokenizer import AlphabeticTokenizer
--- a/py_stringmatching/tokenizer/qgram_tokenizer.py
+++ b/py_stringmatching/tokenizer/qgram_tokenizer.py
@@ -1,6 +1,3 @@
-from six import string_types
-from six.moves import xrange
-
 from py_stringmatching import utils
 from py_stringmatching.tokenizer.definition_tokenizer import DefinitionTokenizer
 
@@ -44,9 +41,9 @@
             raise AssertionError('padding is expected to be boolean type')
         self.padding = padding
 
-        if not isinstance(prefix_pad, string_types):
+        if not isinstance(prefix_pad, str):
             raise AssertionError('prefix_pad is expected to be of type string')
-        if not isinstance(suffix_pad, string_types):
+        if not isinstance(suffix_pad, str):
             raise AssertionError('suffix_pad is expected to be of type string')
         if not len(prefix_pad) == 1:
             raise AssertionError("prefix_pad should have length equal to 1")
@@ -103,7 +100,7 @@
             return qgram_list
 
         qgram_list = [input_string[i:i + self.qval] for i in
-                      xrange(len(input_string) - (self.qval - 1))]
+                      range(len(input_string) - (self.qval - 1))]
         qgram_list = list(filter(None, qgram_list))
 
         if self.return_set:
@@ -193,7 +190,7 @@
             AssertionError: If the length of prefix_pad is not one.
 
         """
-        if not isinstance(prefix_pad, string_types):
+        if not isinstance(prefix_pad, str):
             raise AssertionError('prefix_pad is expected to be of type string')
         if not len(prefix_pad) == 1:
             raise AssertionError("prefix_pad should have length equal to 1")
@@ -228,7 +225,7 @@
             AssertionError: If the length of suffix_pad is not one.
 
         """
-        if not isinstance(suffix_pad, string_types):
+        if not isinstance(suffix_pad, str):
             raise AssertionError('suffix_pad is expected to be of type string')
         if not len(suffix_pad) == 1:
             raise AssertionError("suffix_pad should have length equal to 1")
--- a/py_stringmatching/utils.py
+++ b/py_stringmatching/utils.py
@@ -1,7 +1,4 @@
-import functools
 import re
-import six
-import sys
 
 """
 This module defines a list of utility and validation functions.
@@ -26,9 +23,9 @@
 
 
 def sim_check_for_string_inputs(*args):
-    if not isinstance(args[0], six.string_types):
+    if not isinstance(args[0], str):
         raise TypeError('First argument is expected to be a string')
-    if not isinstance(args[1], six.string_types):
+    if not isinstance(args[1], str):
         raise TypeError('Second argument is expected to be a string')
 
 
@@ -58,7 +55,7 @@
 
 def tok_check_for_string_input(*args):
     for i in range(len(args)):
-        if not isinstance(args[i], six.string_types):
+        if not isinstance(args[i], str):
             raise TypeError('Input is expected to be a string')
 
 
--- a/setup.py
+++ b/setup.py
@@ -109,7 +109,6 @@
         packages=packages,
         install_requires=[
             'numpy >= 1.7.0',
-            'six'
         ],
         setup_requires=[
             'numpy >= 1.7.0'                                                   
